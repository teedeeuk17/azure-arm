﻿#Login-Azurermaccount
#>
<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2019 v5.6.157
	 Created on:   	24/01/2019 16:21
	 Created by:   	Tony Kerr
	 Organization: 	RPA-Environment Agency
	 Filename:     	BindsslCert.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
Binds and ssl cert from local store patch and deploys with Octopus Variables
CREATING FILE AND TESTING
# 1 when creating test and validate with standard powershell variables
# 2 when tested convert to octopus variables and test inline for verificiation
# 3 then check in to source control and verify pull request then merge to master and the deploy as part of package
#
#>


[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory=$true)][String]$webappname="#{webappname}",
		[Parameter(Mandatory=$true)][String]$resourcegroup="#{resourcegroup}",
		[Parameter(Mandatory=$true)][String]$fqdn="#{fqdn}",
        [Parameter(Mandatory=$true)][String]$pfxPath ="{pfxPath}",
		[Parameter(Mandatory=$true)][String]$pfxPassword ="#{pfxPassword}"
 )

Write-Host  "Configuring and Binding SSL Cert to Custom Domain Name in DEFRA  PLEASE WAIT....." -ForegroundColor Yellow 

New-AzureRmWebAppSSLBinding -WebAppName "#{webappname}" -ResourceGroupName "#{resourcegroup}" -Name "#{fqdn}" `
-CertificateFilePath "{pfxPath}" -CertificatePassword "#{pfxPassword}"-SslState SniEnabled

Write-Host  "FINISHED Configuring Diagnostics to Storage and Analytics on LMMS WEB RECEIVER DIAGNOSTICS RESOURCE - "  -ForegroundColor Green

# S T I L L - I N - P R O G R E S S #