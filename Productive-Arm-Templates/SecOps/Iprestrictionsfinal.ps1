﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2019 v5.6.157
	 Created on:   	29/01/2019 14:35:21
	 Created by:   	Tony Kerr / MICROSOFT MSFT
	 Organization: 	RPA-Environment Agency
	 Filename:     	IPRestrictionsfinal.ps1
	 Validated      29/01/2019
	===========================================================================
	.DESCRIPTION
		A description of the file.
Set Allow Rules only for Ip restrictions on Sharepoint.

# 1 when creating test and validate with standard powershell variables
# 2 when tested convert to octopus variables and test inline for verificiation
# 3 then check in to source control and verify pull request then merge to master and the deploy as part of package
#
#>                          
		
[CmdletBinding()]
	Param
	(
		 [Parameter(Mandatory=$false)][String]$ResourceGroupName="#{ResourceGroupName}",
		 [Parameter(Mandatory=$false)][String]$Appname="#{Appname}",
         [Parameter(Mandatory=$false)][String]$SubscriptionId="#{SubscriptionId}"                  
	
 )

function AddRules($rulesToAdd) { 
$rules = @() 
foreach ($item in $rulesToAdd) { 
$rule = [PSCustomObject]@{ipAddress = $item.ipAddress ; action = $item.action ; tag = $item.tag ; priority = $item.priority ; name = $item.name ; description = $item.description } 
$rules += $rule 

} 
return $rules 
}

[PSCustomObject]$rulesToAdd = @{ipAddress = "13.107.6.168/32"; action = "Allow" ; tag = "Default" ; priority = "60"; name = "sharepointaddress1"; description = "sharepoint-online-365-datacentre"},` 
@{ipAddress = "13.107.9.168/32"; action = "Allow" ; tag = "Default" ; priority = "61"; name = "sharepointaddress2"; description = "sharepoint-online-365-datacentre"},` 
@{ipAddress = "13.107.136.0/22"; action = "Allow" ; tag = "Default" ; priority = "62"; name = "sharepointaddress3"; description = "sharepoint-online-365-datacentre"},
@{ipAddress = "40.108.128.0/17"; action = "Allow" ; tag = "Default" ; priority = "63"; name = "sharepointaddress4"; description = "sharepoint-online-365-datacentre"},
@{ipAddress = "52.104.0.0/14"; action = "Allow" ; tag = "Default" ; priority = "64"; name = "sharepointaddress5"; description = "sharepoint-online-365-datacentre"},
@{ipAddress = "104.146.128.0/17"; action = "Allow" ; tag = "Default" ; priority = "65"; name = "sharepointaddress6"; description = "sharepoint-online-365-datacentre"},
@{ipAddress = "134.170.200.0/21"; action = "Allow" ; tag = "Default" ; priority = "66"; name = "sharepointaddress7"; description = "sharepoint-online-365-datacentre"},
@{ipAddress = "134.170.208.0/21"; action = "Allow" ; tag = "Default" ; priority = "67"; name = "sharepointaddress8"; description = "sharepoint-online-365-datacentre"},
@{ipAddress = "150.171.40.0/22"; action = "Allow" ; tag = "Default" ; priority = "68"; name = "sharepointaddress9"; description = "sharepoint-online-365-datacentre"},
@{ipAddress = "191.232.0.0/23"; action = "Allow" ; tag = "Default" ; priority = "69"; name = "sharepointaddress10"; description = "sharepoint-online-365-datacentre"}

#Login-AzureRmAccount
#
#Select-AzureRmSubscription -SubscriptionId $SubscriptionId

$APIVersion = ((Get-AzureRmResourceProvider -ProviderNamespace Microsoft.Web).ResourceTypes | Where-Object ResourceTypeName -eq sites).ApiVersions[0]

$WebAppConfig = (Get-AzureRmResource -ResourceType Microsoft.Web/sites/config -ResourceName $AppName -ResourceGroupName $ResourceGroupName -ApiVersion $APIVersion)

$WebAppConfig.Properties.ipSecurityRestrictions = AddRules -rulesToAdd $rulesToAdd

Set-AzureRmResource -ResourceId $WebAppConfig.ResourceId -Properties $WebAppConfig.Properties -ApiVersion $APIVersion -Force


# S T I L L - I N - P R O G R E S S #