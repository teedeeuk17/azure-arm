﻿#>Login-Azurermaccount
<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2019 v5.6.157
	 Created on:   	27/01/2019 08:45:21
	 Created by:   	Tony Kerr
	 Organization: 	RPA-Environment Agency
	 Filename:     	convertdnstofqdn.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
Converts Azure websites dns suffix to custom dns once CNAME is created on public dns registrar
CREATING FILE AND TESTING
# 1 when creating test and validate with standard powershell variables
# 2 when tested convert to octopus variables and test inline for verificiation
# 3 then check in to source control and verify pull request then merge to master and the deploy as part of package
#
#>

[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory=$false)][String]$fqdn}="#{fqdn}",
		[Parameter(Mandatory=$false)][String]$lmmswebreceivername="#{lmmswebreceivername}",
		[Parameter(Mandatory=$false)][String]$resourcegroupname="#{resourcegroupname}",
		[Parameter(Mandatory=$false)][String]$location="#{location}"



Write-Host "Configure a CNAME record that maps #{fqdn} to #{lmmswebreceivername}.azurewebsites.net"

# Add a custom domain name to the web app. 
Set-AzureRmWebApp -Name "#{lmmswebreceivername}" -ResourceGroupName "#{resourcegroupname}"`
-HostNames @($fqdn,"$webappname.productiveits.co.uk")

# Before continuing, go to your DNS configuration UI for your custom domain and follow the 
# instructions at https://aka.ms/appservicecustomdns to configure a CNAME record for the 
# hostname "www" and point it your web app's default domain name.


# S T I L L - I N - P R O G R E S S #