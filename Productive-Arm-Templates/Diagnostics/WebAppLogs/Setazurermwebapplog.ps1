﻿################################################################################################################
#>Login-Azurermaccount
<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2019 v5.6.157
	 Created on:   	27/01/2019 08:45:21
	 Created by:   	Tony Kerr
	 Organization: 	RPA-Environment Agency
	 Filename:     	SetAzureWebAppLogs.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
Sets the diagnostic Logs to Http Container and Diagnostic Containers storage paths and set the permissions with a sas token for each

CREATING FILE AND TESTING
# 1 when creating test and validate with standard powershell variables
# 2 when tested convert to octopus variables and test inline for verificiation
# 3 then check in to source control and verify pull request then merge to master and the deploy as part of package
#
#>

[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory=$false)][String]$DiagnosticsResourceGroup="#{DiagnosticsResourceGroup}",
		[Parameter(Mandatory=$false)][String]$DiagnosticStorageName="#{DiagnosticStorageName}",
        [Parameter(Mandatory=$false)][String]$HttpLmmsResourceNameLower="#{HttpLmmsResourceNameLower}",                         
	    [Parameter(Mandatory=$false)][String]$DiagLmmsResourceNameLower="#{DiagLmmsResourceNameLower}",
        [Parameter(Mandatory=$false)][String]$ResourceGroupName="#{resourcegroupname}", 
        [Parameter(Mandatory=$false)][String]$WebAppName="#{WebAppName1}"

                                 
                           

 )

$sa = Get-AzureRmStorageAccount -ResourceGroupName "#{DiagnosticsResourceGroup}" -Name "#{DiagnosticStorageName}"

New-AzureStorageContainer -Context $sa.Context -Name "#{HttpLmmsResourceNameLower}" -ErrorAction Ignore
$sasToken = New-AzureStorageContainerSASToken -Context $sa.Context -Name "lmmswebsas" -FullUri -Permission rwdl -StartTime (Get-Date).Date -ExpiryTime (Get-Date).Date.AddYears(200)

New-AzureStorageContainer -Context $sa.Context -Name "#{DiagLmmsResourceNameLower}" -ErrorAction Ignore
$sasToken = New-AzureStorageContainerSASToken -Context $sa.Context -Name "lmmsdiagsas" -FullUri -Permission rwdl -StartTime (Get-Date).Date -ExpiryTime (Get-Date).Date.AddYears(200)


$webApp = Get-AzureRmWebApp -ResourceGroupName "#{resourcegroupname}" -Name "#{WebAppName1}"

$appSettings = [ordered]@{}
$webapp.SiteConfig.AppSettings | % { $appSettings[$_.Name] = $_.Value }
$appSettings.DIAGNOSTICS_AZUREBLOBCONTAINERSASURL = [string]$sasToken


Set-AzureRmWebApp -ResourceGroupName "#{resourcegroupname}" -Name "#{WebAppName1}" -AppSettings $appSettings

########################################################################################################################################################################################




#  # # # # # # # # # # # # # # # # # # # # # # # - - - S T I L L - I N - P R O G R E S S  - - - # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 