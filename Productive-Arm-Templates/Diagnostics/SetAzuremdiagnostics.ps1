﻿#>Login-Azurermaccount
<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2019 v5.6.157
	 Created on:   	27/01/2019 08:45:21
	 Created by:   	Tony Kerr
	 Organization: 	RPA-Environment Agency
	 Filename:     	SetAzureRMDiagnostics.ps1
	===========================================================================
	.DESCRIPTION
		A description of the file.
Sets the diagnostic account and Log Analytics storage paths retention 365 days

CREATING FILE AND TESTING
# 1 when creating test and validate with standard powershell variables
# 2 when tested convert to octopus variables and test inline for verificiation
# 3 then check in to source control and verify pull request then merge to master and the deploy as part of package
#
#>

[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory=$false)][String]$resourceid="#{lmmsresourceid}",
		[Parameter(Mandatory=$false)][String]$diagnosticstorageid="#{diagnosticstorageid}",
        [Parameter(Mandatory=$false)][String]$workspaceid="#{workspaceid}"                           
	
 )
#############################################################################################################################################################################################################

# SETTING AZURE DIAGNOSTICS FOR LMMS WEB APP RECEIVER AND LOG ANALYTICS

##############################################################################################################################################################################################################
Write-Host  "Configuring Diagnostics to Storage and Analytics in DEFRA TEST -  PLEASE WAIT....." -ForegroundColor Yellow 

Set-AzureRmDiagnosticSetting -ResourceId "#{lmmsresourceid}" -StorageAccountId "#{diagnosticstorageid}" -Enabled $True -WorkspaceId "#{workspaceid}"  -RetentionEnabled $True -RetentionInDays 365 
 
Write-Host  "FINISHED Configuring Diagnostics to Storage and Analytics on LMMS WEB RECEIVER DIAGNOSTICS RESOURCE - "  -ForegroundColor Green
