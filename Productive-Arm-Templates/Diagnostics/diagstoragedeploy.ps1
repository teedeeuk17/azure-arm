﻿#>Login-Azurermaccount
<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2019 v5.6.157
	 Created on:   	27/01/2019 08:45:21
	 Created by:   	Tony Kerr
	 Organization: 	RPA-Environment Agency
	 Filename:     	diagstoragedeploy.ps1
	 Validated		29/01/2019	===========================================================================
	.DESCRIPTION
		A description of the file.
Deploys a Diagnostic Storage Account to Ops Subscription for Log Anlaytics
CREATING FILE AND TESTING
# 1 when creating test and validate with standard powershell variables
# 2 when tested convert to octopus variables and test inline for verificiation
# 3 then check in to source control and verify pull request then merge to master and the deploy as part of package
#
#>

[CmdletBinding()]
	Param
	(
		[Parameter(Mandatory=$true)][String]$diagnosticstoragename="#{diagnosticstoragename}",
		[Parameter(Mandatory=$true)][String]$resourcegroup="#{resourcegroup}",
		[Parameter(Mandatory=$true)][String]$location="#{location}",
        [Parameter(Mandatory=$true)][String]$storageskuname="#{storageskuname}"
		                         
 )

Write-Host  "Configuring Diagnostics to Storage Account - $location  PLEASE WAIT....." -ForegroundColor Yellow 


New-AzureRMStorageAccount -StorageAccountName "#{diagnosticstoragename}" -ResourceGroupName "#{resourcegroup}" -Location "#{location}" -SkuName "#{storageskuname}"
Write-Host "FINISHED Configuring Diagnostics to Storage" -ForegroundColor Green



