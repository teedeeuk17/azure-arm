﻿                                             
                       
$resourcegroupName                       
Add-AzureRmMetricAlertRule -TargetResourceId /subscriptions/7e7e93d6-a09e-4afd-a5b3-a2c4a6534248/resourceGroups/NE-PROD-AZUREPAAS/providers/Microsoft.Sql/servers/ne-prod-sql01/databases/neprodprimary `
-Name "ne-prodprimary-database-dtu [Critical]" -ResourceGroup "NE-PROD-AZUREPAAS" -Operator GreaterThan -Threshold 80 -WindowSize 00:05:00 -MetricName dtu_consumption_percent -Description "Database DTU % critical" `
-TimeAggregationOperator Total -Location "west europe"
                     
                     
                               